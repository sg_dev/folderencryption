'''
Created on Jul 2, 2014

@author: stefan

Encrypt: cryptoCLI.py -e <folder-name>

Decrypt: cryptoCLI.py -d <encrypted-file-name>

(Note: both encrypted-file-name and folder-name may be relativ
'''
from getpass import getpass
from optparse import OptionParser
import os
import sys

from myCrypto import Crypto, OperationFailedException


def get_pw_with_gui(newPW=False):
	"""
		newPW ... ask for a new password (for encryption)
	"""
	global password  # FIXME this is maybe not the best solution
	password = ""
	
	#Gui Things
	window = Tk()
	window.resizable(width=FALSE, height=FALSE)
	window.title("Enter a password")
	if newPW:
		window.geometry("200x150")
	else:
		window.geometry("200x90")

	#Creating the username & password entry boxes
	pw_label = Label(window, text="Enter your password:")
	pw_input = Entry(window, show="*")
	pw_label.pack()
	pw_input.pack()
	if newPW:
		pw_confirm_label = Label(window, text="Confirm your password:")
		pw_confirm_input = Entry(window, show="*")
		pw_confirm_label.pack()
		pw_confirm_input.pack()		
		
	def validate():
		global password
		if newPW:
			if pw_input.get() == pw_confirm_input.get():
				#print "both pws match!"
				password = pw_input.get()
			else:
				tkMessageBox.showinfo("-- ERROR --", "Both passwords must match!", icon="error")
				return
		else:
			password = pw_input.get()
		#print password
		window.destroy() # Note: if it was a normal password request (one typing of password)
						 # nothing must be checked

	#ok button
	ok_button = Button(text="OK", command=validate)	
	ok_button.pack()
	
	#Main Starter
	window.mainloop()
	
	#print ".."
	#print password
	return password

def getpass2():
    """
    Prompt for a passwd twice, returning the string only when they
    match
    """
    p1 = getpass('Password: ')
    p2 = getpass('Confirm: ')
    if p1!=p2:
        print >> sys.stderr, '\nPasswords do not match.  Try again.\n'
        return getpass2()
    else:
        return p1

if __name__=='__main__':
    usage = "usage: %prog [options] RootDirectory/filename" 
    parser = OptionParser(usage = usage)
    
    parser.add_option("-e", "--encrypt",
                      action="store_true", dest="encrypt", default=False,
                      help="Encrypt all files of a directory")
    
    parser.add_option("-d", "--decrypt",
                      action="store_true", dest="decrypt", default=False,
                      help="Decrypt all files of an encrypted directory")
    
    parser.add_option("-s", "--secure-remove", dest="sremove", 
                      help="Remove source files after encryption/decryption")
    
    parser.add_option("-v", "--verbose",
                      action="store_true", dest="verbose", default=False,
                      help="Get verbose output from program")
                      
    parser.add_option("-g", "--gui",
                    action="store_true", dest="gui", default=False,
                    help="User for password prompts a GUI")
    
    (options, args) = parser.parse_args()
    
    if len(args) != 1:
        parser.error("Incorrect number of arguments!")
        
    rootDir = args[0]
    if options.verbose: print "RootDir (given by user): %s" % rootDir
    
    if options.encrypt and options.decrypt:
        parser.error('Cannot encrypt and decrypt simultaneously!')
    if not (options.encrypt or options.decrypt):
        parser.error('No option given: must be --encrypt (-e) or --decrypt (-d)!')
    if options.encrypt and not os.path.isdir(rootDir):
        parser.error('Root directory given is no directory!')
    if options.sremove and not (options.sremove == "fast" or
                                options.sremove == "secure" or options.sremove == "mediumsecure" or
                                options.sremove == "highsecure" or options.sremove == "veryhighsecure"):
        parser.error("Allowed Values for secure remove: fast|secure|mediumsecure|highsecure|veryhighsecure")
    
    if options.gui:
        from Tkinter import Label, Button, FALSE, Tk, Entry
        import tkMessageBox

        if options.encrypt: passwd = get_pw_with_gui(newPW=True)
        else: passwd = get_pw_with_gui(newPW=False)
    else:
        if options.encrypt: passwd = getpass2()
        else: passwd = getpass()
    
    crypto = Crypto(verbose = options.verbose, debug = False)
    try:
        if options.encrypt:
            crypto.encryptDir(rootDir, passwd = passwd, sremove = options.sremove)
        elif options.decrypt:
            crypto.decryptDir(rootDir, passwd = passwd, sremove = options.sremove)
    except OperationFailedException as ex:
        if options.gui:
            tkMessageBox.showinfo("-- ERROR --", ex.value, icon="error")
            
    if options.gui and crypto.get_error_count() > 0:
        tkMessageBox.showinfo("-- ERROR --", crypto.get_errors(), icon="warning")
        
    
        
        
        
        
        
