#!/usr/local/bin/python2.7
# encoding: utf-8
"""
    Notes: empty folder will be removed
    
    veryify secure delete: extundelete (linux)  https://wiki.archlinux.org/index.php/File_recovery#Extundelete
                    e.g: extundelete /dev/sda5 --restore-file <absolutPathToFile>
                         extundelete /dev/sda5 --restore-directory <absolutPathToDirectory>
                         extundelete /dev/sda5 --restore-all
                         
    Known Issues:
    
    
"""
import __builtin__
from datetime import datetime
from hashlib import sha1, md5
import os
from subprocess import call
import sys
import tempfile
import platform
from zipfile import ZipFile, ZIP_DEFLATED, BadZipfile
import shutil

from Crypto import Random
from Crypto.Cipher import AES


ENCRYPTION_MODE = AES.MODE_CBC
PROPERTIES_HEADER = "DeAdBeeFFeeBdAeD"


def listFiles(root, patterns='*', recurse=1, return_folders=0):
    # from Parmar and Martelli in the Python Cookbook
    import os.path, fnmatch
    # Expand patterns from semicolon-separated string to list
    pattern_list = patterns.split(';')
    # Collect input and output arguments into one bunch
    class Bunch:
        def __init__(self, **kwds): self.__dict__.update(kwds)
    arg = Bunch(recurse=recurse, pattern_list=pattern_list,
        return_folders=return_folders, results=[])

    def visit(arg, dirname, files):
        #print "visit called..."
        # Append to arg.results all relevant files (and perhaps folders)
        for name in files:
            #print name
            fullname = os.path.normpath(os.path.join(dirname, name))
            #print fullname
            if arg.return_folders or os.path.isfile(fullname):
                for pattern in arg.pattern_list:
                    if fnmatch.fnmatch(name, pattern):
                        arg.results.append(fullname)
                        break
        # Block recursion if recursion was disallowed
        if not arg.recurse: files[:]=[]

    os.path.walk(root, visit, arg)

    return arg.results

class VirtualFilePath(object):
    def __init__(self, inpath, password, iv):
        self.inpath = inpath
        self.password = password
        self.iv = iv
        
    def open(self, *params):
        return BufferedEncryptionFile(self.inpath, self.password, self.iv)
    def stat(self):
        return os.stat(self.inpath)
        
    
class BufferedEncryptionFile(object):
    """
        implementation of an encryption file
        - it supports read operation like normal file except that it returns the
        encrypted content
        - used for the zip file (as implemenation with temporary files does not work with large files)
    """
    def __init__(self, path, password, iv):
        self.inp = open(path, "rb")
        self.enc = AES.new(password, ENCRYPTION_MODE, iv)
        
    def __enter__(self):
        return self
    
    def __exit__(self, type, value, traceback):
        self.close()
        
    def read(self, size):
        data = self.inp.read(size)
        if not data: return ""
        
        length = len(data)
        while (length%16) != 0:  # FIXME - this may cause file corruption
                                # this must only be done at the end -> exploit this
            data += " "
            length+=1
        msg = self.enc.encrypt(data)
        
        return msg
        
    def close(self):
        self.inp.close()
    
stat_pass = os.stat
open_pass = __builtin__.open

def stat_hook(path):
    if isinstance(path, VirtualFilePath):
        return path.stat()
    else:
        return stat_pass(path)
    
def open_hook(path, *params):
    if isinstance(path, VirtualFilePath):
        return path.open(*params)
    else:
        return open_pass(path, *params)
    
class ZipHooks(object):
    hookCount = 0
    
    def __init__(self):
        if not ZipHooks.hookCount:
            os.stat = stat_hook
            __builtin__.open = open_hook
        ZipHooks.hookCount += 1
        
    def __enter__(self):
        return self
    
    def __exit__(self, type, value, traceback):
        self.unhook()
        
    def unhook(self):
        ZipHooks.hookCount -= 1
        if not ZipHooks.hookCount:
            os.stat = stat_pass
            __builtin__.open = open_pass
    

class OperationFailedException(Exception):
        def __init__(self, value):
            self.value = value
        def __str__(self):
            return repr(self.value)
            
class PropertiesFileEntry:
    def __init__(self):
        self.path = ""          # absolut_file_path_from_root
        self.path_hash = ""     # hash of file path
        self.file_hash = ""     # hash value of the file (for integrity check)
        self.padd_len = 0       # length of the padding which must be added to the file
                                # to have a multiple of 16 bytes
        
    def get_file_entry_as_string():
        pass

class Crypto:
    def __init__(self, verbose = False, debug = False):
        self.verbose = verbose
        self.debug = debug
        self.errors = []
        
    def get_errors(self):
        return "\n".join(self.errors)
        
    def get_error_count(self):
        return len(self.errors)
    
    """
        indir: path to a directory which should be enrypted
        passwd: the password
        sremove: String which defines the remove mode
                    must be in fast|secure|mediumsecure|highsecure|veryhighsecure
                    None means archive file will not be removed
    """    
    def encryptDir(self, indir, passwd = 'password', sremove = None):
        """
            password <= 32 Zeichen (Rest wird ergänzt)
            32 Zeichen key (AES-256)
            
        """
        BEGIN_TIME = datetime.now().replace(microsecond=0)
        
        passwd = pwdPadding(passwd)
        
        if self._ends_with_pathsep(indir):
            indir = indir[0:len(indir)-1]
            
        if not os.path.isabs(indir):
            indir = os.path.abspath(indir)
        
        dirName = os.path.basename(indir)
        parentDir = os.path.dirname(indir)
        
        if self.verbose or self.debug:
            print "indir: " + indir
            print "dirName: " + dirName
            print "parentDir: " + parentDir
        
        if not os.path.isdir(indir):
            print >> sys.stderr, '%s is not a directory' % indir
            raise OperationFailedException('%s is not a directory' % indir)
        
        fileList = listFiles(indir, return_folders = 0) #files only
        
        # Windows bug fix with Thumbs.db file
        fileList = [x for x in fileList if not 'Thumbs.db' in x]
        
        fileDict = {dirName + os.sep + os.path.relpath(f,indir): [md5(f).hexdigest(), \
            calculate_hash_of_file(f), str((16 - (os.stat(f).st_size%16)) % 16)] for f in fileList}
        archiveName = parentDir + os.sep + md5(indir).hexdigest() # archiveName is a absolut path to a file, not a directory
        # here could also be used a user supplied value
            
        #save file information
        #
        # fileDict has now this form: {string:list}
        # -> {absolut_path_from_root_infile: [md5_hash_file_name, sha1_hash_file, padd_length]}
        #   Note: the hash of the file will be used for integrity check when decrypting
        #
        prop_str_lst = [PROPERTIES_HEADER, "\n"]
        for f,k in fileDict.iteritems():
            if self.debug: print f + " " + k
            prop_str_lst.extend([f, ",", k[0], ",", k[1], ",", k[2], "\n"])
        prop_str = "".join(prop_str_lst)
        length = len(prop_str)
        mod_len = length % 16
        if mod_len != 0:
            padd_len = 16 - mod_len
        else:
            padd_len = 0
        prop_str = "".join([prop_str, " " * padd_len])
        #print prop_str
        
        iv = Random.new().read(AES.block_size)
        enc = AES.new(passwd, ENCRYPTION_MODE, iv)
        msg = enc.encrypt(prop_str)
        del prop_str
        if self.debug: print "Opening properties file..."        
        
        archive = ZipFile(file = archiveName, mode = "w", compression = ZIP_DEFLATED, allowZip64 = True)
        
        self._add_data_to_archive(iv, archive, "prop")
        self._add_data_to_archive(msg, archive, "properties")
        # save encrypted files
        for infile,lst in fileDict.iteritems():
            if self.verbose or self.debug: print "Encrypting file %s..." % infile
            self._encrypt(parentDir + os.sep + infile, lst[0], passwd, iv, archive)
            
        archive.close()
            
        #optional remove source files
        if not sremove is None:
            self._secure_remove_rec(indir, optstr = sremove)
            
        END_TIME = datetime.now().replace(microsecond=0)
        
        print "Operation successfully completed in " + str(END_TIME - BEGIN_TIME)
    
    """
        hd: md5 hash of the filename [NOT the file] ( will be the new filename )
        archive: a zip archive
        padd_len: the padd_len = the number of bytes which must be added to ensure that the 
        size of the file's content is a multiple of 16
    """    
    def _encrypt(self, path, hd, password, iv, archive):        
        stat = os.stat(path)
        
        if platform.system() == 'Windows' or stat.st_size > 3500000000:     # size > 3.5 GB
            #    code without tmp files (tmp files in Linux faster)
            #    in linux only for large files
            with ZipHooks():
                f = VirtualFilePath(path, password, iv)
                archive.write(filename = f, arcname = hd)
            
        else:   
            # old implementation with tmp files
            # does not work on linux with large files (e.g. > 4 GB)
            inp = open(path, 'rb')
            (fd, abspath) = tempfile.mkstemp()
            out = os.fdopen(fd, "wb")
            
            enc = AES.new(password, ENCRYPTION_MODE, iv)
            while 1:
                data = inp.read(1024)
                if len(data)==0: break
                length = len(data)
                while (length%16) != 0:
                    data += " "
                    length+=1
                msg = enc.encrypt(data)
                out.write(msg)
                
            inp.close()
            out.close()
            
            archive.write(filename = abspath, arcname = hd)
            
            os.unlink(abspath)      # remove temporary file
            

    """
        infile: absolut path to zip archive file
        passwd: the password
        sremove: String which defines the remove mode
                    must be in fast|secure|mediumsecure|highsecure|veryhighsecure
                    None means archive file will not be removed
    """
    def decryptDir(self, infile = '', passwd = 'password', sremove = None):
        # TODO check if it is a zipped file
        """
            password <= 32 Zeichen (Rest wird ergänzt)
            32 Zeichen key (AES-256)
            
        """
        BEGIN_TIME = datetime.now().replace(microsecond=0)
        
        passwd = pwdPadding(passwd)
        
        if not os.path.isabs(infile):
            infile = os.path.abspath(infile)
        
        if self.verbose: print "Infile: %s" % infile
        
        try:
            archive = ZipFile(file = infile, mode = "r", compression = ZIP_DEFLATED, allowZip64 = True) # "a" to append, "r" to read  
        except BadZipfile:
            print >> sys.stderr, 'FATAL ERROR: %s is no valid archive file! aborting...' % infile
            raise OperationFailedException('FATAL ERROR: %s is no valid archive file! aborting...' % infile)
        
        iv = archive.read('prop')
        propStr = archive.read('properties')
        
        if propStr is None or iv is None:
            print >> sys.stderr, 'FATAL ERROR: No properties file found in %s! aborting...' % infile
            raise OperationFailedException('FATAL ERROR: No properties file found in %s! aborting...' % infile)
           
        
        dec = AES.new(passwd, AES.MODE_ECB)
        #iv = dec.decrypt(enc_iv)
        
        dec = AES.new(passwd, ENCRYPTION_MODE, iv)
        csvContent = dec.decrypt(propStr).strip()
        
        #print csvContent
        #print ""
        rows = csvContent.split("\n")
        # check first row in order to see if password was correct
        headerToCheck = rows[0]
        if headerToCheck != PROPERTIES_HEADER:
            error_str = "[FATAL ERROR] The given password was wrong!"
            print >> sys.stderr, error_str
            raise OperationFailedException(error_str)
        rows = rows[1:]
        # form of decryptDict: <string:list>
        # -> <hash_value_of_filename: [absolut_path_from_root_of_archive, hash_value_of_file] >
        #   note: the hash_value_of_file will be used for integrity check
        decryptDict = {}
        for row in rows:
            row_parts = row.split(",")
            name = row_parts[0]
            hd = row_parts[1]
            decryptDict[hd] = [name, row_parts[2], int(row_parts[3])]
            
        outdir = os.path.dirname(infile) 
        if not os.path.isdir(outdir):
            if self.debug or self.verbose: print "Making dirs for %s..." % outdir
            os.mkdir(outdir)
        for hd,lst in decryptDict.iteritems():
            in_file = archive.open(hd, "rU")
            
            outd = outdir + os.sep + lst[0] # lst[0] == filename (absolut from archive root) 
            if self.debug or self.verbose:
                #print "####################"
                #print "Source File  " + ind
                print "Creating " + outd
            self._decrypt(in_file, outd, passwd, iv, lst[1], lst[2]) # lst[1] == hash value of file (for integrity check)
                                                                     # lst[2] == padd_len (must be removed in decrypted file)
            
        archive.close()
            
        #optional remove source files
        if not sremove is None:
            self._secure_remove_file(infile, optstr = sremove)
           
        END_TIME = datetime.now().replace(microsecond=0)
        
        print "Operation successfully completed in " + str(END_TIME - BEGIN_TIME)
    
    """
        inp: file object opened for reading
        outpath: path of outputfile which will be created
        passwd: the password
        iv: initialization vector for AES
    """        
    def _decrypt(self, inp, outpath, passwd, iv, hash_of_file, padd_len):
        outParent = os.path.dirname(outpath)
        if not os.path.isdir(outParent):
            if self.debug or self.verbose: print "Making directories for %s..." % outParent
            os.makedirs(outParent)
        
        #inp  = open(inpath, 'rb')
        out = open(outpath, 'wb')
        dec = AES.new(passwd, ENCRYPTION_MODE, iv)
        
        while 1:
            data = inp.read(1024)
            if len(data)==0: break
            #dec.feed(data)
            msg = dec.decrypt(data)
            out.write(msg)
        
        inp.close()
        out.close()
        
        #remove padding from outfile
        if padd_len > 0:
            with open(outpath, "r+") as out:
                #print "paddlen: %d" % (- padd_len)
                out.seek(- padd_len, os.SEEK_END)
                out.truncate()
        
        #integrity check
        hash_of_new_file = calculate_hash_of_file(outpath)
        if hash_of_new_file != hash_of_file:
            error_str = "[ERROR] The hashes for file %s do not match (%s != %s)!" % (outpath, hash_of_file, hash_of_new_file)
            print error_str
            self.errors.append(error_str)
        
    def _ends_with_pathsep(self, fname):
        """
        Return true if string fname ends in a path separator string
        
        if path ends in a slash, tail will be empty. 
        """
        head, tail = os.path.split(fname)
        del head
        #if self.debug: "Path %s ends with pathsep: %s" % fname % str(tail == "")
        return (tail == "")
        
    def _secure_remove_rec(self, root, optstr = "fast"):
        fileList = listFiles(root)
        fileList = [x for x in fileList if not 'Thumbs.db' in x]
        for f in fileList:
            self._secure_remove_file(f, optstr)
            
        if self.verbose or self.debug: print "Removing dirs recursively"
        dirList = listFiles(root, return_folders=1)
        dirList = [x for x in dirList if not 'Thumbs.db' in x]
        for d in dirList:
            if self.verbose or self.debug: print "Removing directory %s..." % d
            shutil.rmtree(d, ignore_errors = True)
        if self.verbose or self.debug: print "Removing directory %s..." % root
        shutil.rmtree(root, ignore_errors = True)
        
    def _secure_remove_file(self, path, optstr):
        if self.verbose or self.debug:
                print "deleting %s..." % path
        if optstr == "fast":
            secure_delete(path, optstr = optstr)
        else:
            if optstr == "veryhighsecure":
                secure_delete(path, 35, optstr)
            elif optstr == "highsecure":
                secure_delete(path, 7, optstr)
            elif optstr == "mediumsecure":
                secure_delete(path, 3, optstr)
            elif optstr == "secure":
                secure_delete(path, 1, optstr)
            else:
                print >> sys.stderr, """ERROR: Given optstr %s is invalid! 
                                        Valid are [fast|secure|mediumsecure|highsecure|veryhighsecure]
                                        aborting...""" % optstr
                raise OperationFailedException("""ERROR: Given optstr %s is invalid! 
                                        Valid are [fast|secure|mediumsecure|highsecure|veryhighsecure]
                                        aborting...""" % optstr)
    
    """
        datastring: data as string which will be added
        archive: the zip archive where the data will be added
        filename: the filename, a string
    """
    def _add_data_to_archive(self, datastring, archive, filename):
        archive.writestr(filename, datastring)

def calculate_hash_of_file(infile, hashfunc = sha1):
    """
        calculates the hash of the given file infile using the given
        hashfunction hashfunc (e.g. md5, sha1, sha512, etc)
    """
    hashobj = hashfunc()
    BLOCKSIZE = 65536
    with open(infile, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hashobj.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hashobj.hexdigest()
        
def secure_delete(file_path, passes = 1, optstr = "srm"):
    if not optstr == "fast":
        if passes == 7: opt = "-D"          #-D: US Dod compliant 7-pass overwrite. ## too long
        elif passes == 3: opt = "-P" """    OpenBSD compatible rm.  
                                            Files are overwritten three times, 
                                            first with  the byte 0xff, then 0x00, 
                                            and then 0xff again, before they are deleted. """
        elif passes == 35: opt = "-G"       # Use the 35-pass Gutmann method. (very slow)
        else: opt = "-s"                    #-s: simple: 1-pass overwrite
        
        try:
            call(["srm", opt, file_path])  
        except OSError as e:
            print >> sys.stderr, '%s', e     
            #only execute if srm fails
            my_secure_delete(file_path)
    else: # optstr == "fast"
        my_secure_delete(file_path)
        
def my_secure_delete(file_path, passes = 6):
    for i in xrange(0,passes):
        del i
        f = open(file_path, "wb")
        f.write("*"*os.path.getsize(file_path))
        f.flush()
        os.fsync(f)
        f.close()
    f = open(file_path, "wb")
    f.truncate()
    f.flush()
    os.fsync(f)
    f.close()  
    (head, name) = os.path.split(file_path)
    del name
    name = "0000000000"
    newPath = head + os.sep + name
    os.rename(file_path, newPath)
    os.unlink(newPath)
    
        
def pwdPadding(password):
    length  = len(password)
    
    if length > 32:
        print >> sys.stderr, 'password too long: no more than 32 characters allowed'
        return
    
    while length != 32:
        password += " "
        length+=1
        
    return password
   
    
    
    