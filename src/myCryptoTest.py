# -*- coding: utf-8 -*-
'''
Created on Jul 1, 2014

@author: stefan

        UNITTESTS 
'''
import os
import unittest

from myCrypto import secure_delete, Crypto, listFiles


class Test(unittest.TestCase):
    workingDir = os.getcwd() + "/testdir/test"
    
    def testSecureDelete(self):
        # /home/stefan/workspace/FileEncrypt/src/blubb
        testfile = self.workingDir + "/blubb"
        tf = open(testfile, 'wb')
        tf.write("Das ist ein äußerst geheimer Text der nicht gefunden werden darf!!!!!")
        tf.close()
        
        self.assertTrue(os.path.exists(testfile))
        
        secure_delete(testfile, 5)
        
        self.assertFalse(os.path.exists(testfile))
        
    def testEncryptDir(self):
        crypto = Crypto()
        
        crypto.encryptDir(self.workingDir)
        ####
        fileList = listFiles(self.workingDir)
        for f in fileList:
            print "deleting %s..." % f            
            secure_delete(f)
            self.assertFalse(os.path.exists(f))
        ####
        crypto.decryptDir(os.path.dirname(self.workingDir) + "/10510fb5b028ef671ceb20dc74463eac")
        for f in fileList:
            self.assertTrue(os.path.exists(f))
        
def getContentOfFile(path):
    inp = open(path, "rb")
    result = ""
    while 1:
        data = inp.read(1024)
        if len(data)==0: break
        result += data
    inp.close()
        
    return result


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
