# README #

This is a small program for encrypting/decrypting folders with all its files. The easiest way is to use its commandline (cryptoCLI.py).


## Usage ##

- Encrypt a folder (and all its content):

```
#!bash

cryptoCLI.py -e <folder-name>
```

- Decrypt an encrypted file:

```
#!bash

cryptoCLI.py -d <encrypted-file-name>
```

(Note: both encrypted-file-name and folder-name may be relativ)



- both commands could add the *'-s'* option with one of the following parameters:
	[fast|secure|mediumsecure|highsecure|veryhighsecure]
 in order to securely remove the folder/file after encryption/decryption
- *Note:* mostly fast should be used as the other options require the program *srm* installed which mostly won't be the case (the fast option is always used as an fallback if *srm* could not be found)

e.g.
```
#!bash

cryptoCLI.py -es <remove parameter> <folder-name>
```

- there is also a *'-g'* option in order to use a GUI for password input
	-> TODO: this could/should also be used for output (ERRORs etc)
	
e.g.
```
#!bash

cryptoCLI.py -eg <folder-name>
```

## Dependencies ##

- python 2.7
- pycrypto 2.6

_________

- (optional) cx_Freeze in order to generate binary distributions


## How to run tests ##

- myCryptoTest.py
- TODO

## Deployment instructions ##

Currently there are two options to install and run this program

### Option 1 ###

- needs a preinstalled python 2.7
- the simply call the programm from the commandline, e.g. $>python cryptoCLI.py <other arguments...> (after it was cloned from this repo)
- Note: You may need to add python 2.7 to the path (especially in Windows)

### Option 2 ###

- needs a preinstalled cx_Freeze
- then simply call $> cx_Freeze cryptoCLI.py in order to generate the distribution binary, required dlls etc. in the **dist folder**
- Note: the whole dist folder is required in order to run the program


## Additional Notes ##

- the program also had/has a GUI using python-kivy
- with this GUI the app can also be built for android (using buildozer)
- needs pyinstaller to build
- this was tested under Linux, but unfortunately not documented
- to browse the code checkout kivy-gui-android

## Who do I talk to? ##

- the repo owner Stefan Gschiel (stefan.gschiel.sg@gmail.com)